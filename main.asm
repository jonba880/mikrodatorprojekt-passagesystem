.def	param0 = r23
.def	param1 = r24
.def	param2 = r19
.def	lcd_data = r11
.def	tmp = R27
.def	counter = r26
.def	linepointercounter = r18
.def	TAG_LOOP_COUNT = r25
.def	TIMESTAMP = r25

.def    secs = r20 
.def    mins = r17
.def    hrs = r24

.def	QUOTIENT = r16
.def	REMAINDER = r23

.equ	DIODE_MASK = 0x10	; 0001 0000
.equ	FUNCTION_SET_MASK = 0x38	; 0011 1000
.equ	ENTRY_MODE_MASK = 0x06		; 0000 0110
.equ	DISPLAY_MASK = 0x0c			; 0000 1100
.equ	CLEAR_MASK = 0x01			; 0000 0001
.equ	RETURN_TO_HOME_MASK = 0x02	; 0000 0010

.equ	rfid_en_pin = PD1
.equ	rfid_sout_pin = PD0 

.equ	en_pin = PB7
.equ	rw_pin = PB6
.equ	rs_pin = PB5
.equ	diode_pin = PB4
.equ	blip_pin = PB3

.org	$300
tags:
    .db "2200533788",0	;B30
    .db "0000000000",0	;B31
    .db "220053432D",0	;B32
    .db "22005367E2",0	;B33
    .db "2200533DF8",0	;B34
    .db "22005340BD",0	;B35

.org	$200
welcome_message: .db  "User B3",0
notallowed_message: .db "Not Authorized! ",0

.org	$250
LINE: .db "0000000000"

.dseg
TIME: .byte 4

.cseg

.org	$000
rjmp	RESET

.org	$00E
rjmp	TIMERINT

.org	$016
rjmp	USARTINT



RESET:
clr	r16
clr	r17
clr	r18
clr	r19
clr	r21
clr	r22 
clr	r26
clr	r27
clr	r20
clr	r28 
clr	r29
clr	r30
clr	r31

ldi	r16,$FF
out	DDRA,r16
ldi	r16,$FF
out	DDRB,r16
ldi	r16,$FF
out	DDRD,r16

ldi	r16,HIGH(RAMEND)	; Set stackpointer
out	SPH,r16
ldi	r16,LOW(RAMEND)
out	SPL,r16

ldi     YH,HIGH(TIME*2) ; Set Ypointer to point to TIME
ldi     YL,LOW(TIME*2)  ;

/* 
TIMER_INIT
Initierar Timer1.
*/

TIMER_INIT:
	ldi     r16,$00          ; init timer1
	out     TCNT1H,r16             
	out     TCNT1L,r16
	ldi  	r16,(0<<TOIE0) | (0<<TOIE1) | (1 << OCIE1A)	; enabletimer 1
	out  	TIMSK,r16
	ldi	r16, $3
	out	OCR1AH , r16
	ldi	r16, $D1
	out	OCR1AL, r16
	ldi    r16, (1 << WGM12 | 1 << CS12 | 0 << CS11 | 1 << CS10)  
	out    TCCR1B, R16    
	ldi	r16, (1<<ISC01) | (0<<ISC00) | (1<<ISC11) | (0<<ISC10)  
	out	MCUCR, r16

/* 
TIMER_INIT
Initierar USART_INIT.
*/

USART_INIT:
	rcall  RFID_LOW_ENABLE
	ldi	r16,(1<<RXC | 0 << UDRE)
	out	GICR, r16
	rcall	LCD_INIT
	ldi	r16,$0
	out	PORTD,r16
	ldi	r16,0
	out	UBRRH, r16
	ldi	r16,25		; UBRR = 25 => bps = 2400
	out	UBRRL, r16
	ldi	r16, (1<<RXEN) |(0<<TXEN)  | (1<<RXCIE ) | (0<<UDRIE )
	out	UCSRB,r16
	ldi	r16, (1<<URSEL)|(3<<UCSZ0) | (0 << UMSEL)
	out	UCSRC,r16
sei
LOOP:
jmp 	LOOP

/* 
DIV10 (in out param0, out QUOTIENT)
Dividerar param0 med 10. Funktionen returnerar sedan param0 som är resten av divisionen samt QUOTIENT som är kvoten. 
*/

DIV10:
    clr     QUOTIENT
DIV10_START: 
    cpi     param0, $A
    brsh    SUBTRACT10
    ret     
SUBTRACT10:
    subi    param0, $A
    inc     QUOTIENT
    jmp     DIV10_START
    ret


/* 
DIV60 (in out param0, out QUOTIENT)
Dividerar param0 med 60. Funktionen returnerar sedan param0 som är resten av divisionen samt QUOTIENT som är kvoten. 
*/

SUBTRACT60:
    subi    param0, 60
    inc     QUOTIENT
    jmp		DIV60_START
    ret
DIV60:
	clr		QUOTIENT
DIV60_START: 
    cpi     param0, 60
    brsh    SUBTRACT60
    ret     

/* 
SLEEP_N_MS (in param0, in param1)
Fördröjningsfunktion som kallar SLEEP_1_MS ett antal gånger. SLEEP_1_MS exekveras i en loop som räknar N gånger, där N i det här fallet är param0.
*/

SLEEP_N_MS:
push 	param0
push 	param1
clr 	param1
SLEEP_N_MS_LOOP:	rcall 	SLEEP_1_MS
inc 	param1
cp 	param1, param0
brlo 	SLEEP_N_MS_LOOP
pop 	param1
pop 	param0
ret

/* 
SLEEP_1_MS (in param0, in param1) 
Fördröjningsfunktion som slösar 990 klockcykler. (6 + (4 * 41) * (3 * 2)) = 990 klockcykler = 0,000978.
*/

SLEEP_1_MS:
    push 	param0
    push 	param1
    clr 	param0
SLEEP_1_MS_LOOP1:	clr 	param1
SLEEP_1_MS_LOOP2:	inc 	param1
    cpi 	param1, 3
    brne 	SLEEP_1_MS_LOOP2
    inc 	param0
    cpi 	param0, 41
    brne 	SLEEP_1_MS_LOOP1
    pop 	param1
    pop 	param0
    ret


; ***************** LCD FUNCTIONS *********************


/* 
E_EQUALS_1
Sätter enablesignalen(en_pin) för LCDn till 1
*/

E_EQUALS_1:
clr		tmp
in		tmp,PORTB
sbr		tmp, en_pin
out		PORTB,tmp
ret

/* 
E_EQUALS_1
Sätter enablesignalen(en_pin) för LCDn till 0
*/

E_EQUALS_0:
clr		tmp
in		tmp,PORTB
cbr		tmp, en_pin
out		PORTB,tmp
ret

/* 
RS0_RW1
Sätter RS-signalen till 0 och RW-signalen till 1. 
*/

RS0_RW1:	
clr		tmp
in		tmp,PORTB
cbr		tmp, rs_pin
sbr		tmp, rw_pin
out		PORTB,tmp
ret

/* 
RS0_RW1
Sätter RS-signalen till 0 och RW-signalen till 0. 
*/

RS0_RW0:	
clr		tmp
in		tmp,PORTB
cbr		tmp, rs_pin
cbr		tmp, rw_pin
out		PORTB,tmp
ret

/* 
CLEAR_LCD
Använder “clearmasken” för att rensa LCDn.
*/

CLEAR_LCD:
ldi		param0, 2
rcall		SLEEP_N_MS
ldi		param1, CLEAR_MASK		; 0000 0001 clear display
rcall		LCD_INIT_WRITE
ret

/* 
LCD_INIT
Initierar LCDn
*/

LCD_INIT:
push 		param0
push 		param1
ldi		param1, FUNCTION_SET_MASK		; 0011 1000	function set, 8bit data, 2 lines
rcall		LCD_INIT_WRITE
ldi		param1, DISPLAY_MASK		; 0000 1100 display is turned on
rcall		LCD_INIT_WRITE
ldi		param1, CLEAR_MASK		; 0000 0001 clear display
rcall		LCD_INIT_WRITE
ldi		param0, 2
rcall		SLEEP_N_MS
ldi		param1, ENTRY_MODE_MASK		; 0000 0110 cursor moves right
rcall		LCD_INIT_WRITE
ldi		param1, RETURN_TO_HOME_MASK		; 0000 0010  return to home
rcall		LCD_INIT_WRITE
pop 		param1
pop 		param0
ret


/* 
BLIP_NOISE
Skriver 1 till blip_pin och genererar ett ljud.
*/

BLIP_NOISE:
sbi		PORTB, blip_pin
ldi		param0, 100
rcall		SLEEP_N_MS
cbi		PORTB, blip_pin
ret

/* 
DIODE_LIGHT_ON
Skriver 1 till diode_pin. Tänder lampan.
*/

DIODE_LIGHT_ON:
sbi		PORTB, diode_pin
ret

/* 
DIODE_LIGHT_OFF
Skriver 0 till diode_pin. Släcker lampan.
*/

DIODE_LIGHT_OFF:
cbi		PORTB, diode_pin
ret

/* 
LCD_INIT_WRITE(in param1)
Används för att skriva initieringsinstruktioner till LCDn. Param1 innehåller den instruktion som ska skickas.
*/

LCD_INIT_WRITE:
mov 	lcd_data, param1
cbi 	PORTB, rs_pin
cbi 	PORTB, rw_pin
rcall	LCD_WRITE
ret

/* 
LCD_DISPLAY_CHAR (in param1)
Används för att skriva en karaktär till LCDn. Param1 innehåller den karaktär som ska skrivas ut. 
*/

LCD_DISPLAY_CHAR:
mov 	lcd_data, param1
sbi 	PORTB, rs_pin
cbi 	PORTB, rw_pin
rcall	LCD_WRITE
ret

/* 
LCD_WRITE(in param1)
Hjälpfunktion för att skriva till LCDn. Kan användas med olika inställningar för RS och RW.
*/


LCD_WRITE:
sbi 	PORTB, en_pin
rcall 	SLEEP_1_MS
out 	PORTA, lcd_data
rcall 	SLEEP_1_MS
cbi 	PORTB, en_pin
rcall 	SLEEP_1_MS
ret

/* 
LCD_WRITE_LINE(in param0, in param2)
Skriver en sträng till LCDn. Strängen ligger i Z-pekaren och param2 är längden av den strängen. Param0 används endast till att flytta pekaren. 
*/

LCD_WRITE_LINE:
push	param0
push	param2
clr		param0
LCD_WRITE_LINE_LOOP:
    clr		counter
    add		ZL,param0
    lpm		r16,Z
    sub		ZL, param0
    mov		param1, r16
    rcall	LCD_DISPLAY_CHAR
    inc		param0
    cp		param0, param2
    brlo	LCD_WRITE_LINE_LOOP
pop		param0
pop		param2
ret
; ***************** END LCD FUNCTIONS *********************



; ***************** RFID FUNCTIONS ************************


/* 
RFID_HIGH_ENABLE
Sätter en-signalen för RFID till hög.
*/

RFID_HIGH_ENABLE:
    sbi		PORTD, rfid_en_pin
    ret

/* 
RFID_LOW_ENABLE
Sätter en-signalen för RFID till låg.
*/
RFID_LOW_ENABLE:
    cbi		PORTD, rfid_en_pin
    ret


/* 
USARTINT
Usartint är avbrottsrutinen för när en byte läses in från Parallax RFID. När den läst in 12 bytes, det vill säga ett kort-id, jämför den inläst id med befintliga id:n i minnet.
*/

USARTINT:
push	r21
in	r21, SREG
in	param1, UDR
cpi	r19, $C
brsh	USARTINT_END
inc	r19
cpi	r19, $1
breq	USARTINT_END
add	YL, linepointercounter
std	Y+0, param1
sub	YL, linepointercounter  

/* 
USARTINT_INCREASE_ONE_BYTE 
Stödfunktion till USARTINT. Flyttar endast pekare och ser ifall alla bytes är inlästa.
*/

USARTINT_INCREASE_ONE_BYTE: 
ldi	tmp, 1
add	linepointercounter,tmp 
cpi	r19, $0B
breq	USARTDONE

/* 
USARTINT_END
Stödfunktion till USARTINT. Innan avbrottet avslutas, sparas tiden ner då kortet läses in. */

USARTINT_END:
mov	param0, secs
ldi	tmp, 3
add	param0, tmp
rcall	div60
mov	TIMESTAMP, REMAINDER
out	SREG, r21
pop	r21
reti

/* 
USARTDONE
Usartdone jämför inläst kortid med idn i minnet. Ifall inläst kort finns i minnet ges behörighet till användaren, annars informeras användaren att kortet ej har behörighet. 
*/

USARTDONE:
rcall		RFID_HIGH_ENABLE
clr		r16
clr		linepointercounter
rcall		COMPARETAGS
mov		r28, r16
cpi		r28, $9
breq		NOT_ALLOWED
cpi		r28, $9
brlo		WELCOME_SCREEN

/* 
WELCOME_SCREEN
Funktion för att skriva ut att användaren är behörig. Den skriver meddelandet i formatet “User B[kort-nummer] Sek:Min”.
*/

WELCOME_SCREEN:
	ldi		ZH, high(welcome_message << 1)
	ldi		ZL, low(welcome_message << 1)
	ldi		param2, 7
	rcall		LCD_WRITE_LINE
	ldi		tmp, $30			;	Write card char
	mov		param1, r28
	add		param1, tmp
	rcall		LCD_DISPLAY_CHAR

	ldi		param1, $20			; Space 
	rcall		LCD_DISPLAY_CHAR
	ldi		param1, $20			; Space 
	rcall		LCD_DISPLAY_CHAR
	ldi		param1, $20			; Space 
	rcall		LCD_DISPLAY_CHAR

	mov     	param0, mins		; hour 2
	rcall   	DIV10
	ldi		tmp, $30
	mov		param1, QUOTIENT
	add		param1, tmp
	rcall		LCD_DISPLAY_CHAR
	ldi		tmp, $30			; hour 1
	mov		param1, REMAINDER
	add		param1, tmp
	rcall		LCD_DISPLAY_CHAR

	ldi		param1, $3A			
	rcall		LCD_DISPLAY_CHAR

	mov     	param0, secs		; minute 2
	rcall    	DIV10
	ldi		tmp, $30
	mov		param1, QUOTIENT
	add		param1, tmp
	rcall		LCD_DISPLAY_CHAR
	ldi		tmp, $30			; minute 1
	mov		param1, REMAINDER
	add		param1, tmp
	rcall		LCD_DISPLAY_CHAR

	call    	DIODE_LIGHT_ON
	call    	BLIP_NOISE
	jmp		USARTDONE_END

/* 
NOT_ALLOWED
Funktion som informerar användaren om att kortet ej är behörig. Skriver ut innehållet i strängen notallowed_message, det vill säga “Not Authorized”. 
*/

NOT_ALLOWED:
ldi		ZH, high(notallowed_message << 1)
ldi		ZL, low(notallowed_message << 1)
ldi		param2, $F
rcall		LCD_WRITE_LINE
ldi		param1, $20			; Space 
rcall		LCD_DISPLAY_CHAR
jmp		USARTDONE_END

/* 
USARTDONE_END
Återställer UDR-registret samt övriga relevanta register inför nästa inläsning från kortläsaren. 
*/

USARTDONE_END:
rcall		CLEAR_REGISTERS
in		r16,UDR
in		r16,UDR
in		r16,UDR
jmp		USARTINT_END
reti


/* 
CLEAR_REGISTERS
Rensar register.
*/

CLEAR_REGISTERS:
clr		r16
clr		r18
clr		r19
clr		r21
clr		r22
clr		r23
clr		r25
clr		r26
clr		r27
ldi		YH,HIGH(LINE*2) 	
ldi		YL,LOW(LINE*2)	
ret

/* 
COMPARETAGS (in param0, in param1)
Jämför inläst kort med kort-idn i minnet. Pekare Y innehåller inläst kort-id. Pekare Z pekare på starten av listan med kort-idn.
*/

COMPARETAGS: ; Y = TAG FROM CARD, Z = TAGS FROM MEMORY
push		param0
push		param1
clr		TAG_LOOP_COUNT
clr		r16
clr		param0
ldi		TAG_LOOP_COUNT, 0
ldi		ZH, high(tags<< 1)
ldi		ZL, low(tags << 1)
COMPARETAGS_LOOP_1:
	add		ZL, param0
	clr		param0
COMPARETAGS_LOOP_2:
add		ZL,param0
add		YL,param0
lpm		r16, Z
ldd		param1, Y+0
sub		ZL,param0
sub		YL, param0
inc		param0
cp			r16, param1
brne		COMPARETAGS_RETURN_FALSE
cpi		param0, 10
brlo		COMPARETAGS_LOOP_2

COMPARETAGS_SUCCESS:
mov		r16, TAG_LOOP_COUNT
	jmp		COMPARETAGS_END

COMPARETAGS_RETURN_FALSE:
	ldi		r16, 9
	inc		TAG_LOOP_COUNT
	ldi		param0, 12
	cpi		TAG_LOOP_COUNT, 6	; <- 6 = number of strings
	brlo		COMPARETAGS_LOOP_1

COMPARETAGS_END:
	pop		param1
	pop		param0
ret



; ***************** END RFID FUNCTIONS ********************

; ***************** START CLOCK FUNCTION ********************

/* 
RESET_usart 
Funktionen släcker skärmen och lampan. Den sätter även enablesignalen för RFID till låg, så nästa kort kan läsas in. 
*/

RESET_USART:
rcall		CLEAR_LCD
rcall		RFID_LOW_ENABLE
rcall		DIODE_LIGHT_OFF
jmp		COUNT_SEC_2

/* 
COUNT_SEC/COUNT_SEC2
Räknar upp antal sekunder.
*/

COUNT_SEC:
inc		secs
cp		secs, TIMESTAMP	
breq	RESET_USART 
COUNT_SEC_2:
cpi		secs, 59
brlo	COUNT_END
clr		secs

/* 
COUNT_MIN
Räknar upp antalet minuter.
*/

COUNT_MIN:
inc		mins
cpi		mins, 60
brlo	COUNT_END
clr		mins


COUNT_END:
ret 

; ***************** END CLOCK FUNCTION ********************

/* 
TIMERINT
Avbrottsrutinen som hanterar klockan. Med hjälp av CTC-mode på TIMER1 räknar den upp en sekund varje gång den körs.
*/

TIMERINT: 
push	r16
push	param0
in		r16, SREG
rcall	COUNT_SEC
out		SREG, r16
pop		param0
pop		r16
reti
